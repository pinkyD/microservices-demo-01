
## 创建镜像
```
make
```
或
```
docker build -t userlogin:0.0.1 .
```

## 启动容器
```
make run
```
或
```
docker run --name userlogin -p 8001:8001 --link userdb  -e MYSQL_HOST="userdb" -e MYSQL_DB="my_db"  -d userlogin:0.0.1
```
## 进入容器
```
docker exec -it CONTAINERID sh

```


## 可能用到的命令
```
go mod init userlogin
go mod tidy
```