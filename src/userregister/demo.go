package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

//Ret ...
type Ret struct {
	Code int    `json:"code"`
	Data string `json:"data"`
}

func doHello(w http.ResponseWriter, r *http.Request) {
	ret := new(Ret)
	ret.Code = 200

	ret.Data = "用户注册接口"

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	retJSON, _ := json.Marshal(ret)
	io.WriteString(w, string(retJSON))
}
func main() {
	fmt.Println("hello go!")
	http.HandleFunc("/", doHello)     //设置访问的路径
	http.ListenAndServe(":8002", nil) //设置监听的端口
}
