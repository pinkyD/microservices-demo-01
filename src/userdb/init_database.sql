-- 建库
CREATE DATABASE IF NOT EXISTS my_db default charset utf8 COLLATE utf8_general_ci;

-- 切换数据库
use my_db;

-- 建表
DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`username` varchar(100) DEFAULT NULL COMMENT '用户名',
`password` varchar(100) DEFAULT NULL COMMENT 'MD5密码',
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 插入数据
INSERT INTO `user` (`id`, `username`, `password`)
VALUES
(1,'user01','e10adc3949ba59abbe56e057f20f883e'),
(2,'user02','e10adc3949ba59abbe56e057f20f883e');