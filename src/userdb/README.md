
## 创建镜像
```
make
```
或
```
docker build -t userdb:0.0.1 .
```

## 启动容器
```
make run
```
或
```
docker run --name userdb -e MYSQL_ROOT_PASSWORD=123456 -d userdb:0.0.1
```
## 通过命令行操作数据库
```
docker exec -it CONTAINERID bin/bash

mysql -uroot -p123456

show databases;
use my_db;
select * from user;

exit

```

## 初始化数据库脚本
当Mysql容器首次启动时，会在 /docker-entrypoint-initdb.d目录下扫描 .sh，.sql，.sql.gz类型的文件。如果这些类型的文件存在，将执行它们来初始化一个数据库。

如果有多个sql文件，无法保证执行顺序，这就需要引入shell脚本文件，在docker-entrypoint-initdb.d 目录下放置 sh 文件，在 sh 文件中依次执行 sql 文件。
