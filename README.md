# 微服务示例项目

## 介绍
在线选宿舍DEMO


## 手动部署

1.  下载代码  
```
git clone https://gitee.com/fineaiops/microservices-demo-01.git
```
2.  构建并运行镜像
```
用户数据库
 cd ~/microservices-demo-01/src/userdb/
 make
 make run

用户登录服务
 cd ~/microservices-demo-01/src/userlogin/
 make
 make run

 http://IP:8001/login

用户注册服务
 cd ~/microservices-demo-01/src/userregister/
 make
 make run

 http://IP:8002
```



3. 运行nginx反向代理
```
 cd ~/microservices-demo-01/nginx-proxy/

 docker run -p 80:80 --add-host=myserver:主机IP --name nginx-proxy -v $PWD/www:/www -v $PWD/conf/conf.d:/etc/nginx/conf.d -v $PWD/logs:/wwwlogs -v /etc/localtime:/etc/localtime --restart=always  -d nginx
```


## 镜像操作
### 停止所有的container
docker stop $(docker ps -a -q)

### 删除所有container
docker rm $(docker ps -a -q)

### 删除全部image
docker rmi -f $(docker images -q)
